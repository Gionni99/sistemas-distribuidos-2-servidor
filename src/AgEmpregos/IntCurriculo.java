/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

/**
 * Interface do curriculum
 * @author Giovanni e Steven
 */
public class IntCurriculo {
    
    //String para area de interesse
    String areaDeInteresse;
    //Referência do Cliente2
    InterfaceCli2 cliente;
    
    /**
     * Construtor
     * @param areaDeInteresse
     * @param cliente 
     */
    
    public IntCurriculo(String areaDeInteresse, InterfaceCli2 cliente) {
        this.areaDeInteresse = areaDeInteresse;
        this.cliente = cliente;
    }
    
}
