/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;


/**
 * Classe servente do servidor
 * @author Giovanni e Steven
 */
public class ServImpl extends UnicastRemoteObject implements InterfaceServ{
    
    //listas dos dados principais
    private List<Curriculo> curriculos;
    private List<VagaEmprego> vagas;
    //listas do interesses
    private List<IntVaga> interessesVagas;
    private List<IntCurriculo> interessesCurriculos;
    
    /**
     * Construtor
     * @throws RemoteException 
     */
    
    public ServImpl() throws RemoteException{//construtor que inicializa as listas que serão utilizadas
        this.curriculos = new ArrayList<Curriculo>();
        this.vagas = new ArrayList<VagaEmprego>();
        this.interessesVagas = new ArrayList<IntVaga>();
        this.interessesCurriculos = new ArrayList<IntCurriculo>();
    }
    
    
    /**
     * ConsultaVaga pesquisa uma vaga que  tenha a area de interesse semelhante à fornecida na função
     * Primeira de três versões desta função
     * @param areaDeInteresse
     * @param cliente  
     * @throws RemoteException 
     */
    @Override
    public void ConsultaVaga(String areaDeInteresse, InterfaceCli1 cliente) throws RemoteException{
        VagaEmprego vagaAtual;
        int encontrada = 0;
        for(int i=0;i<vagas.size();i++){
            vagaAtual=vagas.get(i);
            if(vagaAtual.areaDaVaga.startsWith(areaDeInteresse)){
                cliente.Chamada("[[Vaga Encontrada: "+
                        vagaAtual.nomeDaEmpresa+"/"+
                        vagaAtual.contato+"/"+
                        vagaAtual.areaDaVaga+"/"+
                        vagaAtual.cargaHoraria.toString()+"/"+
                        vagaAtual.salario.toString()+"]]");
                encontrada = 1;
            }
        }
        if(encontrada == 0){
            cliente.Chamada("[[Nenhuma Vaga Encontrada Com Essa Area de Interesse]]");
        }
    }
    /**
     * ConsultaVaga pesquisa uma vaga que tenha como salario  minimo o recebido na função
     * Segunda versão de três desta função
     * @param salarioPretendido
     * @param cliente
     * @throws RemoteException 
     */
    @Override
    public void ConsultaVaga(Integer salarioPretendido, InterfaceCli1 cliente) throws RemoteException{
        VagaEmprego vagaAtual;
        int encontrada = 0;
        for(int i=0;i<vagas.size();i++){
            vagaAtual=vagas.get(i);
            if(vagaAtual.salario>=salarioPretendido){
                cliente.Chamada("[[Vaga Encontrada: "+
                        vagaAtual.nomeDaEmpresa+"/"+
                        vagaAtual.contato+"/"+
                        vagaAtual.areaDaVaga+"/"+
                        vagaAtual.cargaHoraria.toString()+"/"+
                        vagaAtual.salario.toString()+"]]");
                encontrada = 1;
            }
        }
        if(encontrada == 0){
            cliente.Chamada("[[Nenhuma Vaga Encontrada Com Esse Salario Minimo]]");
        }
    }
    
    /**
     * ConsultaVaga por área E salário
     * Terceira versão de três desta função
     * @param areaDeInteresse
     * @param salarioPretendido
     * @param cliente
     * @throws RemoteException 
     */
    @Override
    public void ConsultaVaga(String areaDeInteresse, Integer salarioPretendido, InterfaceCli1 cliente) throws RemoteException{
        VagaEmprego vagaAtual;
        int encontrada = 0;
        for(int i=0;i<vagas.size();i++){
            vagaAtual=vagas.get(i);
            if(vagaAtual.salario>=salarioPretendido && vagaAtual.areaDaVaga.startsWith(areaDeInteresse)){
                cliente.Chamada("[[Vaga Encontrada: "+
                        vagaAtual.nomeDaEmpresa+"/"+
                        vagaAtual.contato+"/"+
                        vagaAtual.areaDaVaga+"/"+
                        vagaAtual.cargaHoraria.toString()+"/"+
                        vagaAtual.salario.toString()+"]]");
                encontrada = 1;
            }
        }
        if(encontrada == 0){
            cliente.Chamada("[[Nenhuma Vaga Encontrada Com Essa Area de Interesse e Esse Salario Minimo]]");
        }
    }
    
    /**
     * Consulta  de curriculum  pela área de interesse
     * @param areaDeInteresse
     * @param cliente
     * @throws RemoteException 
     */
    @Override
    public void ConsultaCurriculo(String areaDeInteresse, InterfaceCli2 cliente) throws RemoteException{
        Curriculo curriculoAtual;
        int encontrada = 0;
        for(int i=0;i<curriculos.size();i++){
            curriculoAtual=curriculos.get(i);
            if(curriculoAtual.areaDeInteresse.startsWith(areaDeInteresse)){
                cliente.Chamada("[[Curriculo Encontrado: "+
                        curriculoAtual.nome+"/"+
                        curriculoAtual.contato+"/"+
                        curriculoAtual.areaDeInteresse+"/"+
                        curriculoAtual.cargaHoraria.toString()+"/"+
                        curriculoAtual.salarioPretendido.toString()+"]]");
                encontrada = 1;
            }
        }
        if(encontrada == 0){
            cliente.Chamada("[[Nenhuma Vaga Encontrada Com Essa Area de Interesse e Esse Salario Minimo]]");
        }
    }
    /**
     * Cadastro e alteração do curriculum pelo Cliente1
     * @param nome
     * @param contato
     * @param areaDeInteresse
     * @param cargaHoraria
     * @param salarioPretendido
     * @param clienteRef
     * @throws RemoteException 
     */
    @Override
    public void CadastroCurriculo(String nome,String contato,String areaDeInteresse, Integer cargaHoraria, Integer salarioPretendido, InterfaceCli1 clienteRef) throws RemoteException{
        Curriculo curriculum = new Curriculo(nome,contato,areaDeInteresse,cargaHoraria,salarioPretendido,clienteRef);
        
        Curriculo curriculoAtual;
        int encontrada = 0;
        for(int i=0;i<curriculos.size();i++){
            curriculoAtual=curriculos.get(i);
            if(curriculoAtual.cliente.equals(clienteRef)) {
                curriculos.set(i, curriculum);
                encontrada = 1;
            }
        }
        if(encontrada==0){
            curriculos.add(curriculum);
        }
        
        clienteRef.Chamada("[[Cadastro concluído com sucesso!]]");
        
        for(int i=0;i<interessesCurriculos.size();i++){
            if(interessesCurriculos.get(i).areaDeInteresse.startsWith(areaDeInteresse)){
                interessesCurriculos.get(i).cliente.Chamada("[[ENCONTRADO NOVO CURRICULO]]\n"+
                        "[["+
                        nome+"/"+
                        contato+"/"+
                        areaDeInteresse+"/"+
                        cargaHoraria.toString()+"/"+
                        salarioPretendido.toString()+
                        "]]"
                        );
            }
        }
    }
    
    /**
     * Cadastro e alteração das vagas ofertadas pelo Cliente2
     * @param nome
     * @param contato
     * @param areaDeVaga
     * @param cargaHoraria
     * @param salario
     * @param clienteRef
     * @throws RemoteException 
     */
    @Override
    public void CadastroVaga(String nome, String contato, String areaDeVaga, Integer cargaHoraria, Integer salario, InterfaceCli2 clienteRef) throws RemoteException{
        VagaEmprego vagaEmprego = new VagaEmprego(nome,contato,areaDeVaga,cargaHoraria,salario,clienteRef);
        
        VagaEmprego vagaAtual;
        int encontrada = 0;
        for(int i=0;i<vagas.size();i++){
            vagaAtual=vagas.get(i);
            if(vagaAtual.cliente.equals(clienteRef)) {
                vagas.set(i, vagaEmprego);
                encontrada = 1;
            }
        }
        if(encontrada==0){
            vagas.add(vagaEmprego);
        }
        
        clienteRef.Chamada("[[Cadastro concluído com sucesso!]]");
        
        for(int i=0;i<interessesVagas.size();i++){
            if(interessesVagas.get(i).areaDeInteresse.startsWith(areaDeVaga)){
                interessesVagas.get(i).cliente.Chamada("[[ENCONTRADO NOVO CURRICULO]]\n"+
                        "[["+
                        nome+"/"+
                        contato+"/"+
                        areaDeVaga+"/"+
                        cargaHoraria.toString()+"/"+
                        salario.toString()+
                        "]]"
                        );
            }
        }
    }
 
    /**
     * Registro de interesse de vagas da área especificada na String 'areaDeInteresse'
     * pelo Cliente1
     * @param areaDeInteresse
     * @param cliente
     * @throws RemoteException 
     */
    @Override
    public void RegistroInteresseVaga(String areaDeInteresse, InterfaceCli1 cliente) throws RemoteException{
        IntVaga NovoInteresse = new IntVaga(areaDeInteresse,cliente);
        interessesVagas.add(NovoInteresse);
        cliente.Chamada("[[Interesse Registrado Com Sucesso!]]");
    }
    
    /**
     * Registro de interesse de curriculos da área especificada na String 'areaDeInteresse'
     * pelo Cliente2
     * @param areaDeInteresse
     * @param cliente
     * @throws RemoteException 
     */
    @Override
    public void RegistroInteresseCurriculo(String areaDeInteresse,InterfaceCli2 cliente) throws RemoteException{
        IntCurriculo NovoInteresse = new IntCurriculo(areaDeInteresse,cliente);
        interessesCurriculos.add(NovoInteresse);
        cliente.Chamada("[[Interesse Registrado com Sucesso!]]");
    }
}
