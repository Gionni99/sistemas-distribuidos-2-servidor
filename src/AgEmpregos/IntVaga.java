/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

/**
 * Interface das vagas
 * @author Giovanni e Steven
 */
public class IntVaga {
    //String de entrada
    String areaDeInteresse;
    //Referência do Cliente1
    InterfaceCli1 cliente;
    
    /**
     * Construtor
     * @param areaDeInteresse
     * @param cliente 
     */
    
    public IntVaga(String areaDeInteresse, InterfaceCli1 cliente) {
        this.areaDeInteresse = areaDeInteresse;
        this.cliente = cliente;
    }
}
