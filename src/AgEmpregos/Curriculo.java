/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

/**
 * Classe para preenchimento do curriculo
 * @author Giovanni e Steven
 */
public class Curriculo {
    //dados do curriculo
    String nome;
    String contato;
    String areaDeInteresse;
    Integer cargaHoraria;
    Integer salarioPretendido;
    //Referência do Cliente1
    InterfaceCli1 cliente;
    
    /**
     * Construtor
     * @param nome
     * @param contato
     * @param areaDeInteresse
     * @param cargaHoraria
     * @param salarioPretendido
     * @param cliente 
     */
    
    public Curriculo(String nome,String contato,String areaDeInteresse, Integer cargaHoraria, Integer salarioPretendido, InterfaceCli1 cliente) {
        this.nome= nome;
        this.contato = contato;
        this.areaDeInteresse = areaDeInteresse;
        this.cargaHoraria = cargaHoraria;
        this.salarioPretendido = salarioPretendido;
        this.cliente = cliente;
    }
    
}
