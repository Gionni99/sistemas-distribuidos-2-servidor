/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Classe do Servidor, utilizado para a gerência das vagas e curriculos
 * @author Giovanni e Steven
 */
public class Servidor {
    /**
     * Função main para execução do lado do servidor
     * @param args the command line arguments
     */
    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        // TODO code application logic here
        ServImpl servidor = new ServImpl();//cria uma nova instancia de ServImpl que será utilizada por todos os processos clientes
        
        Registry registro = LocateRegistry.createRegistry(1099);//cria um novo registro na porta 1099
        registro.bind("Servidor", servidor);//registra o servidor no registro para que possa ser encontrado pelos clientes
    }
    
}
